:mod:`DuckHunt` --- Duckhunt game
=================================

An animal hunting IRC game

.. automodule:: modules.DuckHunt
    :members:
    :undoc-members:
    :show-inheritance:
