:mod:`DogeRPC` --- A dogecoind RPC service
==========================================

This module provides a service for interacting with dogecoind.

.. automodule:: modules.DogeRPC
    :members:
    :undoc-members:
    :show-inheritance:
