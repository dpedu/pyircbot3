:mod:`DogeWallet` --- A Dogecoin wallet
=======================================

This module provides a dogecoin wallet hosted on the IRC bot's server

.. automodule:: modules.DogeWallet
    :members:
    :undoc-members:
    :show-inheritance:
