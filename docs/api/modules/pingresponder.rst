:mod:`PingResponder` --- Service ping responder
===============================================

Module to repsond to irc server PING requests

.. automodule:: modules.PingResponder
    :members:
    :undoc-members:
    :show-inheritance:
