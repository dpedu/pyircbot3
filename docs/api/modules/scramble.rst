:mod:`Scramble` --- Module to provide a word scramble game
==========================================================

yabba blahblah blahblah

.. automodule:: modules.Scramble
    :members:
    :undoc-members:
    :show-inheritance:
