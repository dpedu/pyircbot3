:mod:`MySQL` --- MySQL service
==============================

Module providing a mysql type service

.. automodule:: modules.MySQL
    :members:
    :undoc-members:
    :show-inheritance:
