:mod:`Seen` --- !seen <username>
================================

Provides !seen <username>

.. automodule:: modules.Seen
    :members:
    :undoc-members:
    :show-inheritance:
