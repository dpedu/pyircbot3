:mod:`Error` --- A module to cause an error
===========================================

yabba blahblah blahblah

.. automodule:: modules.Error
    :members:
    :undoc-members:
    :show-inheritance:
