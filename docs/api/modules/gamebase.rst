:mod:`GameBase` --- IRC game codebase
=====================================

A codebase for making IRC games

.. automodule:: modules.GameBase
    :members:
    :undoc-members:
    :show-inheritance:
