:mod:`DogeDice` --- A dogecoin game
===================================

Module to provide a game for gambling Dogecoin

.. automodule:: modules.DogeDice
    :members:
    :undoc-members:
    :show-inheritance:
