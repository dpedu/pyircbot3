:mod:`CryptoWallet` --- BitcoinD RPC Service
============================================

Module to provide a multi-type cryptocurrency wallet

.. automodule:: modules.CryptoWallet
    :members:
    :undoc-members:
    :show-inheritance:
