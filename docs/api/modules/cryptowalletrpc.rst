:mod:`CryptoWalletRPC` --- BitcoinD RPC Service
===============================================

Module capable of operating Bitcoind-style RPC. Provided as a service.

.. automodule:: modules.CryptoWalletRPC
    :members:
    :undoc-members:
    :show-inheritance:
