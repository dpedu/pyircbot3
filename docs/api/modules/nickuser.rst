:mod:`NickUser` --- A module to cause an error
==============================================

A module providing a simple login/logout account service

.. automodule:: modules.NickUser
    :members:
    :undoc-members:
    :show-inheritance:
