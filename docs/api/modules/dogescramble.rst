:mod:`DogeScramble` --- A word scramble game with rewards
=========================================================

This module provides a word scrambling game that rewards winners with small
amounts of Dogecoin

.. automodule:: modules.DogeScramble
    :members:
    :undoc-members:
    :show-inheritance:
