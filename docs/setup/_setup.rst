Setup Tutorial
==============

Installing and runnig PyIRCBot requires installing the needed dependancies and
providing some environmental information about where the bot is being run.

Contents:

.. toctree::
   :maxdepth: 8
   
   dependancies.rst
   initial_config.rst
   running.rst
