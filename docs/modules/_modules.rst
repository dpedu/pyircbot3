Included Modules
================

PyIRCBot includes several modules to add basic functionality to your bot.

Contents:

.. toctree::
   :maxdepth: 8
   :glob:
   
   ../api/modules/*
