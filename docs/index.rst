.. pyircbot3 documentation master file, created by
   sphinx-quickstart on Thu Oct  2 12:02:45 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyIRCBot3 - A minimalist and modular IRC bot
============================================

PyIRCBot is a fault tolerant, multi-threaded, modular IRC bot designed for
Python 3. 

Contents:

.. toctree::
   :maxdepth: 8
   
   setup/_setup.rst
   modules/_modules.rst
   rpc/_rpc.rst
   dev/_dev.rst

More Information
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

